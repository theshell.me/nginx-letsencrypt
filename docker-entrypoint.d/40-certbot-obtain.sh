#!/bin/bash

certbot="certbot"
obtain_args="certonly -n \
  --keep-until-expiring --expand \
  --config /etc/nginx-letsencrypt/certbot.ini \
  --standalone"
#  --webroot -w /var/local/letsencrypt/webroot"
certs_args="certificates -n"
delete_args="delete -n"

readarray -t conf < /etc/nginx-letsencrypt/certificates.conf

echo "Obtaining certificates..."

names=""
for line in "${conf[@]}"; do
  if [ ! -z "$line" ] && [[ "$line" != \#* ]]; then
    name=${line%% *}
    names="$names|$name|"
    echo "Obtaining $line..."
    $certbot $obtain_args --cert-name $line
  fi
done

echo "Deleting unused certificates..."

for name in `$certbot $certs_args | grep 'Certificate Name: ' | awk '{print $3}'`; do
  if [[ ${names} != *"|$name|"* ]]; then
    echo "Deleting $name..."
    $certbot $delete_args --cert-name $name
  fi
done
