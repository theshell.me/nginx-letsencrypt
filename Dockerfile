FROM nginx:1.21-alpine

RUN apk add --no-cache bash certbot

RUN mkdir -p /var/local/letsencrypt/webroot/.well-known

COPY docker-entrypoint.d/* /docker-entrypoint.d/
COPY etc/ /etc/
